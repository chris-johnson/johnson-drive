"""
NAME
    drive -- driving history report

SYNOPSIS
    drive [COMMAND]

    Generate report:
        drive report <INPUT-FILE

    Miscellaneous:
        drive help
        drive usage
        drive version

DESCRIPTION
    Generates a text report listing drivers, miles driven and average speed.
    See the INPUTS section for an example and discussion of the input format.
    Trips with average speed <5 MPH or >100 MPH are excluded.

OPTIONS
    Some parameters have equivalent environment variables.  When a command-
    line parameter is provided, it supersedes the environment variable.

    --help
        Show detailed usage information for the command, and exit.

    --version
        Show the program version number, and exit.

INPUTS
    Each line of input is a space-delimited command. Example:

        Driver Dan
        Driver Alex
        Driver Bob
        Trip Dan 07:15 07:45 17.3
        Trip Dan 06:12 06:32 21.8
        Trip Alex 12:01 13:16 42.0

    The command (field 0) defines the meaning of fields 1+.

    Driver command
        Field 1, "Driver Name"
            A unique ID for a driver in string format, maximum 128 characters.
            Case-insensitive, so "Fred", "FRED" and "fred" are equivalent.

    Trip command
        Field 1, "Driver Name"
            References a driver, usually but not necessarily defined by an
            earlier "Driver" command. See DATA CONSISTENCY section below.

        Field 2, "Start Time"
            Clock time when a trip starts in "hh:mm" format. No time zone.

        Field 3, "Stop Time"
            Clock time when a trip ends in "hh:mm" format. No time zone.
            Input rules dictate start time <= stop time <= midnight.

        Field 4, "MPH"
            Average speed during trip (miles per hour) in decimal format.

OUTPUTS
    Each line of output is a per-driver summary. Example:

        Alex: 42 miles @ 34 mph
        Dan: 39 miles @ 47 mph
        Bob: 0 miles

    This is a text format designed for human-readability. For each driver, the
    miles driven and average MPH are given, rounded to the nearest integer.

EXIT CODES
        Exits with status 0 if all input is processed successfully and the
        report is output successfully, or greater than 0 if errors occur.

            0       Success

            1       Unrecognized command

            2       Unrecognized command format

            3       Invalid field format

            4       Invalid field value

EXAMPLES
    drive report <INPUT-FILE

NOTES
    COMPATIBILITY
        Python >=2.7.9, <3.0

    ENVIRONMENT VARIABLES
        See OPTIONS for additional environment variables.
"""


##----------------------------------------------------------------------------
## future modules
##

from __future__ import absolute_import


##----------------------------------------------------------------------------
## standard modules
##

from types import FunctionType

import io
import sys


##----------------------------------------------------------------------------
## custom modules
##

from fire import helputils

import fire
import pager


##----------------------------------------------------------------------------
## local modules
##

from .metadata import __version__
from . import core


##----------------------------------------------------------------------------
## configurations / pragmas
##


##----------------------------------------------------------------------------
## module constants
##

## -- could be introspected
COMMAND_NAME = 'drive'
ENVVAR_PREFIX = COMMAND_NAME.upper()


##----------------------------------------------------------------------------
## module classes
##

class CLIHandler(object):
    def version(self):
        """
Print version and exit.
        """
        print __version__
    ##
    def help(self):
        """
Show detailed usage in pager.
        """
        with io.BytesIO(__doc__.strip()) as fp:
            pager.page(fp, pagecallback=manpage_prompt)
    ##
    def usage(self):
        """
Print brief usage and quit (cleander than Fire usage).
        """
        print 'Usage:'
        for method in [k for k, v in self.__class__.__dict__.items() if type(v) == FunctionType]:
            print '    ' + COMMAND_NAME + ' ' + method
    ##
    def report(self, data=sys.stdin):
        """
Generate report.

DOCTESTS
    >>> data = '''Driver Dan
    ... Driver Alex
    ... Driver Bob
    ... Trip Dan 07:15 07:45 17.3
    ... Trip Dan 06:12 06:32 21.8
    ... Trip Alex 12:01 13:16 42.0'''
    >>> for line in core.report(data=data):
    ...     print line
    Alex: 42 miles @ 34 mph
    Dan: 39 miles @ 47 mph
    Bob: 0 miles
        """
        for line in core.report(data=data):
            print line
        sys.stdout.flush()


##----------------------------------------------------------------------------
## module functions
##

def manpage_prompt(pagenum):
    if pager.getch() in ['q', 'Q']:
        return False


##-----------------------------------------------------------------------------
## entry points
##

def _main():
    """
Entry point when run as a zipfile or directory (imported in __main__.py), or
as a CLI utility (defined as a Python entry point in the setup.py script).
    """
    if len(sys.argv) < 2:
        sys.argv.append('report')
    fire.Fire(CLIHandler)


##
## entry point when executed directly as a script

if __name__ == '__main__':
    _main()
