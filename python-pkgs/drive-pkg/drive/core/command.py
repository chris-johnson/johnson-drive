"""
Command-handling classes
"""

##----------------------------------------------------------------------------
## future modules
##

from __future__ import print_function


##----------------------------------------------------------------------------
## standard modules
##

from decimal import Decimal

import collections
import csv
import io


##----------------------------------------------------------------------------
## local modules
##

from ..util import json
from ..util import doctest_print


##----------------------------------------------------------------------------
## module functions
##

def timestr_to_minutes(timestr):
    """
Convert strings like "1:06" to minutes count like 76.

DOCTESTS
    >>> timestr_to_minutes('00:00')
    0
    
    >>> timestr_to_minutes('0:0')
    0
    
    >>> timestr_to_minutes('1:00')
    60
    
    >>> timestr_to_minutes('00:03')
    3
    
    >>> timestr_to_minutes('23:59') == 23*60 + 59
    True
    
    >>> timestr_to_minutes(':00')
    Traceback (most recent call last):
        ...
    ValueError: invalid literal for int() with base 10: ...
    
    >>> timestr_to_minutes('00:')
    Traceback (most recent call last):
        ...
    ValueError: invalid literal for int() with base 10: ...
    
    >>> timestr_to_minutes('0000')
    Traceback (most recent call last):
        ...
    ValueError: input must be in "HH:MM" format
    
    >>> timestr_to_minutes(':')
    Traceback (most recent call last):
        ...
    ValueError: invalid literal for int() with base 10: ...

    >>> timestr_to_minutes('27:00')
    Traceback (most recent call last):
        ...
    ValueError: hours must be in 0..23 range

    >>> timestr_to_minutes('-5:00')
    Traceback (most recent call last):
        ...
    ValueError: hours must be in 0..23 range

    >>> timestr_to_minutes('00:73')
    Traceback (most recent call last):
        ...
    ValueError: minutes must be in 0..59 range

    >>> timestr_to_minutes('00:-18')
    Traceback (most recent call last):
        ...
    ValueError: minutes must be in 0..59 range

    >>> timestr_to_minutes('AB:CD')
    Traceback (most recent call last):
        ...
    ValueError: invalid literal for int() with base 10: 'AB'

    >>> timestr_to_minutes(12.34)
    Traceback (most recent call last):
        ...
    TypeError: input must be a string
    """
    ##
    ## explicit check to control exception type
    if not isinstance(timestr, basestring):
        raise TypeError('input must be a string')
    ##
    ## '12:15' --> 12, 15
    parts = timestr.split(':')
    if len(parts) != 2:
        raise ValueError('input must be in "HH:MM" format')
    hours = int(parts[0])
    minutes = int(parts[1])
    if hours < 0 or hours > 23:
        raise ValueError('hours must be in 0..23 range')
    if minutes < 0 or minutes > 59:
        raise ValueError('minutes must be in 0..59 range')
    ##
    ## convert to minutes
    return hours * 60 + minutes


##----------------------------------------------------------------------------
## module classes
##

class BaseCommand(object):
    """
Base class for any command, e.g. Drive or Trip.

INSTANCE DATA
    self.accum -- provides add() for recording validated records.
    """
    def __init__(self, accum, **kwargs):
        self.accum = accum
    ##
    def __call__(self, args):
        self.validate()
        self.save()
    ##
    def validate(self):
        """
Required in derived classes. Validate and possibly transform input.
        """
        raise NotImplementedError
    ##
    def save(self):
        if self.valid:
            self.accum(**self.args)
        else:
            ##
            ## just for testing
            doctest_print('invalid')


class DriverCommand(BaseCommand):
    """
Interpret the Driver command.

INSTANCE DATA
    self.args -- dict w/ input fields and derived fields.

DOCTESTS
    >>> accum = lambda **x: print(json.dumps(x, sort_keys=True))
    >>> dc = DriverCommand(accum=accum)

    >>> dc(['fred'])
    {"name": "Fred"}

    >>> dc(['Fred'])
    {"name": "Fred"}

    >>> dc(['FRED'])
    {"name": "Fred"}

    >>> dc(['FRED', 123])
    Traceback (most recent call last):
        ...
    IndexError: input length expected: 1

    >>> dc([456])
    Traceback (most recent call last):
        ...
    TypeError: name field must be a string
    """
    def __call__(self, args):
        if len(args) != 1:
            raise IndexError('input length expected: 1')
        ##
        self.args = {}
        self.args['name'] = args[0]
        ##
        super(self.__class__, self).__call__(args)
    ##
    def validate(self):
        self.valid = False
        ##
        ## driver name
        if not isinstance(self.args['name'], basestring):
            raise TypeError('name field must be a string')
        self.args['name'] = self.args['name'].title()
        ##
        self.valid = True


class TripCommand(BaseCommand):
    """
Implementation of Trip command.

INSTANCE DATA
    self.args -- dict w/ input fields and their derived fields.

DOCTESTS
    >>> accum = lambda **x: print(json.dumps(x, sort_keys=True))
    >>> tc = TripCommand(accum=accum)

    >>> tc(['fred', '1:02', '2:10', '14'])
    {"miles": "14", "minutes": "68", "mph": null, "name": "Fred", "start": "1:02", "stop": "2:10"}

    >>> tc(['Fred', '11:50', '23:50', '549.2'])
    {"miles": "549.2", "minutes": "720", "mph": null, "name": "Fred", "start": "11:50", "stop": "23:50"}

    ## too slow
    >>> tc(['fReD', '00:00', '12:00', '1.0'])
    invalid

    ## too fast
    >>> tc(['freD', '00:00', '00:05', '100.0'])
    invalid

    >>> tc(['FRED', '4:31', '2:10', '88.9'])
    Traceback (most recent call last):
        ...
    ValueError: stop time must be >= start time

    >>> tc(['FrEd', '2:37', '8:19', '-118.2'])
    Traceback (most recent call last):
        ...
    ValueError: miles must be >= 0

    ##
    ## time format tested more deeply in timestr_to_minutes()
    
    >>> tc(['fred', '4:02', '2:10', '14'])
    Traceback (most recent call last):
        ...
    ValueError: stop time must be >= start time

    >>> tc(['fred', '184:02', '2:10', '14'])
    Traceback (most recent call last):
        ...
    ValueError: hours must be in 0..23 range

    >>> tc(['fred', '4:96', '2:10', '14'])
    Traceback (most recent call last):
        ...
    ValueError: minutes must be in 0..59 range
    """
    def __call__(self, args):
        if len(args) != 4:
            raise IndexError('input length expected: 4')
        ##
        self.args = {}
        self.args['name'] = args[0]
        self.args['start'] = args[1]
        self.args['stop'] = args[2]
        self.args['miles'] = args[3]
        ##
        ## just for doctests
        self.args['minutes'] = None
        self.args['mph'] = None
        ##
        super(self.__class__, self).__call__(args)
    ##
    def validate(self):
        self.valid = False
        ##
        ## driver name
        if not isinstance(self.args['name'], basestring):
            raise TypeError('name field must be a string')
        self.args['name'] = self.args['name'].title()
        ##
        ## time span
        start_minutes = timestr_to_minutes(self.args['start'])
        stop_minutes = timestr_to_minutes(self.args['stop'])
        ##
        ## normalize
        self.args['minutes'] = Decimal(stop_minutes - start_minutes)
        if self.args['minutes'] < 0:
            raise ValueError('stop time must be >= start time')
        ##
        ## miles
        self.args['miles'] = Decimal(self.args['miles'])
        if self.args['miles'] < 0:
            raise ValueError('miles must be >= 0')
        ##
        ## filters
        if self.args['minutes']:
            mph = 60 * self.args['miles'] / self.args['minutes']
            if mph >= Decimal(5) and mph <= Decimal(100):
                self.valid = True
