"""
Reporting support
"""

##----------------------------------------------------------------------------
## future modules
##

from __future__ import print_function


##----------------------------------------------------------------------------
## standard modules
##

from decimal import Decimal

import collections
import csv
import io
import sys


##----------------------------------------------------------------------------
## local modules
##

from ..util import json
from .command import DriverCommand
from .command import TripCommand


##----------------------------------------------------------------------------
## module classes
##

class DataReader(object):
    def __init__(self, data, handler=None):
        """
Read CSV file, calling a callback on each row. This approach allows very large
or streaming data sets to be handled as per the caller's special requirements.

DOCTESTS
    >>> dr = DataReader(data='\\na 1 b 2\\nc 3 d 4')
    >>> for row in dr.read():
    ...     print(row)
    ['a', '1', 'b', '2']
    ['c', '3', 'd', '4']
        """
        self.data = data
        self.handler = handler
        ##
        ## normalize input
        if isinstance(data, basestring):
            self.data = io.BytesIO(self.data)
        ##
        ## create reader
        self.reader = csv.reader(self.data, delimiter=' ')
    ##
    def read(self):
        """
Process individual lines, and pass to callback handler.
        """
        for row in self.reader:
            ##
            ## skip blank lines
            if not ''.join(row).strip():
                continue
            yield row


class TripAccumulator(object):
    """
Build a dict entry per name, and bump trips, miles and minutes as called.

NOTES
    In calls (adds), the "numeric" fields must be Decimals or decimals-as-
    strings, to avoid precision issues. This class converts to Decimal.

DOCTESTS
    >>> ta = TripAccumulator()
    >>> print(json.dumps(ta.trips['John'], sort_keys=True))
    {"miles": "0", "minutes": "0", "trips": 0}

    ##
    ## with preload data

    >>> ta = TripAccumulator({'John': {'trips': 2, 'miles': Decimal(40), 'minutes': Decimal(45)}})
    >>> print(json.dumps(ta.trips['John'], sort_keys=True))
    {"miles": "40", "minutes": "45", "trips": 2}

    >>> ta(name='John', miles='2', minutes='5')
    >>> print(json.dumps(ta.trips['John'], sort_keys=True))
    {"miles": "42", "minutes": "50", "trips": 3}

    >>> ta(name='John', miles=3, minutes='3')
    Traceback (most recent call last):
        ...
    TypeError: miles must be a Decimal, or decimal-as-string

    >>> ta(name='John', miles='3', minutes=3)
    Traceback (most recent call last):
        ...
    TypeError: minutes must be a Decimal, or decimal-as-string

    >>> ta(name='John', miles=Decimal('3'), minutes=Decimal(3))
    >>> print(json.dumps(ta.trips['John'], sort_keys=True))
    {"miles": "45", "minutes": "53", "trips": 4}
    
    ##
    ## test reporter while we're here

    >>> r = Reporter(accum=ta)
    >>> for line in r.report():
    ...    print(line)
    John: 45 miles @ 51 mph

    ##
    ## note if we sorted by pre-rounding miles, the sort order would be Moe,
    ## Curly, John -- but post-rounding the miles are the same, so sub-sort
    ## by name and get Curly, John, Moe


    >>> ta(name='Moe', miles='45.2', minutes='53.1')
    >>> ta(name='Curly', miles='45.1', minutes='52.9')
    >>> ta(name='Larry', miles='121', minutes='104')
    >>> ta(name='Shemp', miles='0', minutes='0')
    >>> for line in r.report():
    ...    print(line)
    Larry: 121 miles @ 70 mph
    Curly: 45 miles @ 51 mph
    John: 45 miles @ 51 mph
    Moe: 45 miles @ 51 mph
    Shemp: 0 miles
    """
    def __init__(self, trips=None):
        self.trips = collections.defaultdict(
            lambda: {'trips': 0, 'miles': Decimal(0.0), 'minutes': Decimal(0.0)},
            trips or {}
        );
    ##
    def __call__(self, name=None, miles='0', minutes='0', **kwargs):
        if not isinstance(miles, (basestring, Decimal)):
            raise TypeError('miles must be a Decimal, or decimal-as-string')
        if not isinstance(minutes, (basestring, Decimal)):
            raise TypeError('minutes must be a Decimal, or decimal-as-string')
        ##
        self.trips[name]['trips'] += 1
        self.trips[name]['miles'] += Decimal(miles)
        self.trips[name]['minutes'] += Decimal(minutes)


class LineHandler(object):
    """
Simple router. Use field 0 to choose handler for fields 1+.

DOCTESTS
    >>> h_a = lambda x: print(json.dumps(x + ['command a'], sort_keys=True))
    >>> h_b = lambda x: print(json.dumps(x + ['command b'], sort_keys=True))
    >>> h_c = lambda x: print(json.dumps(x + ['command c'], sort_keys=True))
    >>> lh = LineHandler({'a': h_a, 'b': h_b, 'C': h_c})

    >>> line = ['a', 1, 'x', 2]
    >>> lh(line)
    [1, "x", 2, "command a"]

    >>> line = ['b', 3, 'y', 4]
    >>> lh(line)
    [3, "y", 4, "command b"]

    >>> line = ['A', 1, 'x', 2]
    >>> lh(line)
    [1, "x", 2, "command a"]

    >>> line = ['c', 5, 'z', 6]
    >>> lh(line)
    [5, "z", 6, "command c"]

    >>> line = ['x']
    >>> lh(line)
    Traceback (most recent call last):
        ...
    ValueError: command not recognized

    >>> line = ['a']
    >>> lh(line)
    ["command a"]

    >>> line = []
    >>> lh(line)
    Traceback (most recent call last):
        ...
    IndexError: list index out of range
    """
    def __init__(self, commands):
        self.commands = {}
        for k, v in commands.items():
            self.commands[k.lower()] = v
    ##
    def __call__(self, line):
        command = line[0].lower()
        if command not in self.commands:
            raise ValueError('command not recognized')
        ##
        self.commands[command](line[1:])


class Reporter(object):
    def __init__(self, accum):
        self.accum = accum
    ##
    def report(self):
        ##
        ## build list to sort
        lines = []
        for k, v in self.accum.trips.items():
            ##
            ## name, as-is
            name = k
            ##
            ## miles, plus format
            miles_num = int(round(v['miles'], 0))
            miles = str(miles_num) + ' miles'
            ##
            ## mph, plus format
            if not v['minutes']:
                mph = ''
            else:
                mph_num = v['miles'] / v['minutes'] * Decimal(60)
                mph_num = int(round(mph_num, 0))
                mph = '@ ' + str(mph_num) + ' mph'
            ##
            ## render output line
            text = '{name}: {miles} {mph}'.format(**locals()).strip()
            ##
            ## store for sorting
            lines += [{'name': name, 'miles': miles_num, 'text': text}]
        ##
        ## sort and return
        for line in sorted(lines, key=lambda x: (-x['miles'], x['name'])):
            yield line['text']


def report(data):
    """
Orchestrate end-to-end report generation.
    """
    accum = TripAccumulator()
    commands = {
        'driver': DriverCommand(accum=accum),
        'trip': TripCommand(accum=accum)
    }
    ##
    handler = LineHandler(commands=commands)
    reader = DataReader(data=data)
    for line in reader.read():
        handler(line)
    ##
    reporter = Reporter(accum=accum)
    for line in reporter.report():
        yield line
