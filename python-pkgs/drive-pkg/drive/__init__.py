"""
Package-level __init__
"""


##----------------------------------------------------------------------------
## future modules
##

from __future__ import absolute_import


##----------------------------------------------------------------------------
## standard modules
##

import logging
import sys


##----------------------------------------------------------------------------
## package config
##

from .metadata import *  ## pylint: disable=wildcard-import


##----------------------------------------------------------------------------
## logging config
## -- general config that's safe when run as a library or a script/app
## -- in library mode, log options should be set in the CALLING module
##

##
## -- safe default for any module type: generic look-up using module name
log = logging.getLogger(__name__)
##
## -- recommended for any importable library, optional for an application
log.addHandler(logging.NullHandler())
##
## -- for any module type, maximize output when within an active doctest
## -- note in a default configuration, loglevel defaults to 30 / WARNING
if hasattr(sys.modules['__main__'], '_SpoofOut'):
    log.setLevel(logging.DEBUG)
##
## -- suppress noisy warnings: urllib3, pycurl, others
logging.captureWarnings(True)
