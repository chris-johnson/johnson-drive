"""
Standard metadata
"""


__author__ = 'Chris Johnson'
__email__ = 'cmj@highway77.com'
__version__ = '1.0.7'
__version_date__ = '29-Nov-2018'
__copyright__ = 'Copyright (c) 2018 Chris Johnson.  All rights reserved.'
__description__ = 'Driving history report - library and CLI.'
