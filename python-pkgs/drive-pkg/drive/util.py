"""
Common utilities
"""

##----------------------------------------------------------------------------
## future modules
##

from __future__ import print_function


##----------------------------------------------------------------------------
## standard modules
##

import decimal
import json
import sys


##----------------------------------------------------------------------------
## module functions
##

##
## monkeypatch JSON to dump less common data types

def json_encode_other(obj):
    if isinstance(obj, decimal.Decimal):
        return str(obj)
    raise TypeError(repr(obj) + ' is not JSON serializable')

old_dumps = json.dumps

def new_dumps(*args, **kwargs):
    kwargs['default'] = json_encode_other
    return old_dumps(*args, **kwargs)

json.dumps = new_dumps


##
## in active doctest session

def in_doctest():
    """
There isn't an official way to detect, so we will look around.
    """
    if '_pytest.doctest' in sys.modules:
        return True
    ##
    if '__main__' in sys.modules:
        ##
        ## direct doctest
        if hasattr(sys.modules['__main__'], '_SpoofOut'):
            return True
        ##
        ## doctest via pytest
        if sys.modules['__main__'].__dict__.get('__file__', '').endswith('/pytest'):
            return True
    ##
    return False


##
## print only in active doctest

def doctest_print(*args, **kwargs):
    if in_doctest():
        print(*args, **kwargs)
