## -*- coding: utf-8 -*-

data = '''Driver Päper
Driver Δ
Driver 字
Trip Päper 07:15 07:45 17.3
Trip Päper 06:12 06:32 21.8
Trip Δ 12:01 13:16 42.0'''

##
## note unexpected title-casing for Unicode

report = '''Δ: 42 miles @ 34 mph
PäPer: 39 miles @ 47 mph
字: 0 miles
'''

from ..cli import CLIHandler

def test_example(capsys):
    CLIHandler().report(data=data)
    out, err = capsys.readouterr()
    assert out.encode('utf8') == report
