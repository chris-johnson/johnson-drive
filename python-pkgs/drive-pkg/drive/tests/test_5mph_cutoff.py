data = '''
Trip Below 1:00 2:00 4.999999
Trip On 3:00 4:00 5.000000
Trip Above 5:00 6:00 5.000001'''

report = '''invalid
Above: 5 miles @ 5 mph
On: 5 miles @ 5 mph
'''

from ..cli import CLIHandler

def test_5mph(capsys):
    CLIHandler().report(data=data)
    out, err = capsys.readouterr()
    assert out == report
