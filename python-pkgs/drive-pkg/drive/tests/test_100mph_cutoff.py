data = '''
Trip Below 1:00 2:00 99.999999
Trip On 3:00 4:00 100.000000
Trip Above 5:00 6:00 100.000001'''

report = '''invalid
Below: 100 miles @ 100 mph
On: 100 miles @ 100 mph
'''

from ..cli import CLIHandler

def test_100mph(capsys):
    CLIHandler().report(data=data)
    out, err = capsys.readouterr()
    assert out == report
