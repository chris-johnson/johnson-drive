data = '''Driver Dan
Driver dAN
Driver Alex
driver ALEX
Driver Bob
driver BoB
Trip DAN 07:15 07:45 17.3
Trip DAn 06:12 06:32 21.8
Trip DaN 07:15 07:45 17.3
Trip Dan 06:12 06:32 21.8
Trip dAN 07:15 07:45 17.3
Trip dAn 06:12 06:32 21.8
Trip daN 07:15 07:45 17.3
Trip dan 06:12 06:32 21.8
Trip Alex 12:01 13:16 42.0'''

report = '''Dan: 156 miles @ 47 mph
Alex: 42 miles @ 34 mph
Bob: 0 miles
'''

from ..cli import CLIHandler

def test_example(capsys):
    CLIHandler().report(data=data)
    out, err = capsys.readouterr()
    assert out == report
