data = '''Driver Bob
Trip Dan 07:15 07:45 17.3
Trip Dan 06:12 06:32 21.8
Trip Alex 12:01 13:16 42.0'''

report = '''Alex: 42 miles @ 34 mph
Dan: 39 miles @ 47 mph
Bob: 0 miles
'''

from ..cli import CLIHandler

def test_fewdrivers(capsys):
    CLIHandler().report(data=data)
    out, err = capsys.readouterr()
    assert out == report
