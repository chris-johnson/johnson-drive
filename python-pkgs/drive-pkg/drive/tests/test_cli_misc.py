from ..cli import CLIHandler
from ..metadata import __version__

def test_usage(capsys):
    CLIHandler().usage()
    out, err = capsys.readouterr()
    assert out.split('\n')[0] == 'Usage:'

def test_version(capsys):
    CLIHandler().version()
    out, err = capsys.readouterr()
    assert out.strip() == __version__
