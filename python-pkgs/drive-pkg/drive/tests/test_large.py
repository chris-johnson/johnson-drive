import os 

DIR = os.path.dirname(os.path.realpath(__file__))

report = '''Dan: 64000000 miles @ 64 mph
Alex: 5600000 miles @ 56 mph
Bob: 0 miles
'''

from ..cli import CLIHandler

def test_large(capsys):
    with open(os.path.join(DIR, './test_large.txt')) as data:
        CLIHandler().report(data=data)
    out, err = capsys.readouterr()
    assert out == report
