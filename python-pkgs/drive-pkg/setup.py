#!/usr/bin/env python

"""
NAME
    setup.py - simplified pip install script

SYNOPSIS
    Install from PyPi-style repo:
        $ pip install NAME

    Install from local files, fixed-in-time version:
        $ pip install $(dirname /PATH/TO/PACKAGE/setup.py)

    Install from local files, live version via symlink:
        $ pip install -e $(dirname /PATH/TO/PACKAGE/setup.py)

    Test embedded libraries + .../tests/test_xxx.py runners:
        $ python setup.py test

DESCRIPTION
    Uses conventions to help make this file more DRY with the package. Looks
    up name, version and other info from the package and project structure, so
    redundant data entry is not needed. When used in a project that uses the
    conventions, only the "module-specific settings" section requires editing.
"""


##-----------------------------------------------------------------------------
## standard libraries
##-----------------------------------------------------------------------------

import errno
import os
import re
import sys

from setuptools import find_packages
from setuptools import setup
from setuptools.command.test import test as TestCommand


##-----------------------------------------------------------------------------
## module config / pragmas
##-----------------------------------------------------------------------------

## pragma: no cover

if sys.version_info < (2,7,9):
    sys.exit('Sorry, your Python version is not supported')


##-----------------------------------------------------------------------------
## module constants
##-----------------------------------------------------------------------------

##
## module-specific settings
##
## -- package name for pip install
## -- for uniqueness in the pip repo
PACKAGE = 'johnson-drive'
##
## -- folder above top-level __init__.py
## -- as used in an import statement
MODULE = 'drive'
##
## -- project repo URL in SCC website
PROJECT = 'https://gitlab.com/chris-johnson/drive'
##
## -- is a CLI provided
MODULE_CLI = True


##
## modules required for testing
TESTLIBS = [
    'pytest',
    'pytest-cache',
    'pytest-sugar',
    'pylint',
    'pytest-pylint',
    'pytest-cov',
]


##
## file types / extensions to exclude
## -- coordinate with gitignore files
BADEXTS = [
    '.pyc',
    '.pyo',
    '.pyd',
]


##-----------------------------------------------------------------------------
## module functions
##-----------------------------------------------------------------------------

def is_sequence(arg):
    """
Identify scalars vs. iterables
    """
    return (hasattr(arg, '__iteritems__')
            if not hasattr(arg, 'strip')
            else hasattr(arg, '__iter__'))


def listify(arg):
    """
Turn a scalar into a list -- leave lists & other iterables alone.
    """
    if is_sequence(arg) and not isinstance(arg, dict):
        return arg
    return [arg,]


def readfile(*names):
    """
Helper utility. Read text from file to help keep setup.py DRY.
    """
    thisfile = os.path.abspath(__file__)
    thisfile = os.path.join(os.path.dirname(thisfile), *names)
    for ext in ['', '.txt', '.md']:
        try:
            with open(thisfile + ext) as fp:
                return fp.read()
        except IOError as exc:
            pass
    ##
    raise exc


def find_metadata(tag=None, *file_paths):
    """
Helper utility. Read metadata from module to help keep setup.py DRY.
    """
    metadata_file = readfile(*file_paths)
    metadata_match = re.search(
        tag + r"\s*\=\s*'(.*)'", metadata_file
    )
    if metadata_match:
        return metadata_match.group(1)
    raise RuntimeError('Cannot find metadata entry: ' + tag)


def reqload(mode, fname='requirements.txt', addl=None):
    """
Read required modules from external text file. OK if not present.

in "pip freeze" output, "-e URL" is used for links to private repos
like "-e git+ssh://git@bitbucket.org/u/r/#egg=e&subdirectory=d", while
public PyPi repos look like "cryptography==1.5.2" -- these need to be
distinguished in the call to setup() -- different parameters.
    """
    prefix = '-e '
    if addl:
        addl = listify(addl)
    ##
    ## this filters in "-e " lines for "dep" mode, and out for "req" mode
    test = lambda x: x.strip().startswith(prefix) == dict(req=0, dep=1)[mode]
    reqs = []
    try:
        reqs = [i.split(prefix)[-1] for i in readfile(fname).splitlines() if test(i)]
    except IOError as exc:
        ##
        ## ok if file not present
        if exc.errno == errno.ENOENT:
            pass
        else:
            raise
    ##
    if addl:
        reqs.extend(addl)
    return reqs


def pipreq(addl):
    """
Requirements for "install_requires" parameter (public repos).
    """
    return reqload(mode='req', addl=addl)


def pipdep(addl):
    """
Requirements for "dependency_links" parameter (SCC repos).
    """
    return reqload(mode='dep', addl=addl)


##-----------------------------------------------------------------------------
## module classes
##-----------------------------------------------------------------------------

class PyTest(TestCommand):
    """
Standard test-runner.
    """
    user_options = []
    ##
    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = []  ## pragma pylint: disable=attribute-defined-outside-init
    ##
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []  ## pragma pylint: disable=attribute-defined-outside-init
        self.test_suite = True  ## pragma pylint: disable=attribute-defined-outside-init
    ##
    def run_tests(self):
        import pytest
        err = pytest.main(self.pytest_args)
        sys.exit(err)


##-----------------------------------------------------------------------------
## main setup script
## -- see https://docs.python.org/2/distutils/setupscript.html
## -- many options are non-obvious and confusingly named!
##-----------------------------------------------------------------------------

setup(
    ##
    ## version info
    name=PACKAGE,
    version=find_metadata('__version__', MODULE, 'metadata.py'),
    ##
    ## author info
    author=find_metadata('__author__', MODULE, 'metadata.py'),
    author_email=find_metadata('__email__', MODULE, 'metadata.py'),
    ##
    ## descriptions
    description=readfile('README')[0],  ## first line only
    long_description=readfile('README'),
    license=readfile('LICENSE'),
    url=PROJECT,
    ##
    ## packages
    packages=find_packages(exclude=['test', 'tests',]),
    namespace_packages=[],
    py_modules=[],
    include_package_data=True,
    ##
    ## dependencies
    ##
    ## -- found in PyPI, or private PyPi (like Gemfury)
    ## -- define here, or in requirements.txt file
    install_requires=pipreq(
        [
            'setuptools>=18.0',
        ]
    ),
    ##
    ## -- found in private SCC repos (like Bitbucket)
    ## -- define here, or in requirements.txt file with "-e "
    dependency_links=pipdep(
        []
    ),
    ##
    ## executables
    entry_points={
        'console_scripts': [
            '{MODULE} = {MODULE}.cli:_main'.format(**locals()),
        ],
    } if MODULE_CLI else {},
    scripts='',
    ##
    ## testing
    test_suite='tests',
    tests_require=TESTLIBS,
    cmdclass={'test': PyTest},
    ##
    ## distribution
    download_url='N/A',
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    provides=[MODULE,],
    obsoletes=[],
    platforms=['Any',],
    zip_safe=False,
)
