drive -- driving history reporting utility

Developer notes. See project-level README.MD for a larger discussion.

---------------------------------------


# Testing and Coverage

During development, you should run regression tests often:

    python ./python-pkgs/drive-pkg/setup.py test

Most of the tests run quickly and provide a lot of info. You can disable the `test_large.py`
test via the .coveragerc file -- that probably only needs to be run before a git push.

Here's an output sample:

    Test session starts (platform: darwin, Python 2.7.15, pytest 4.0.1, pytest-sugar 0.9.2)
    rootdir: /Users/chris/proj/root, inifile: pytest.ini
    plugins: sugar-0.9.2, pylint-0.13.0, cov-2.6.0
    collecting ...
     python-pkgs/drive-pkg/drive/cli.py ✓                                                                                               7% ▊
     python-pkgs/drive-pkg/drive/core/command.py ✓✓✓                                                                                   29% ██▉
     python-pkgs/drive-pkg/drive/core/reporting.py ✓✓✓                                                                                 50% █████
     python-pkgs/drive-pkg/drive/tests/test_0.py ✓                                                                                     57% █████▊
     python-pkgs/drive-pkg/drive/tests/test_100mph_cutoff.py ✓                                                                         64% ██████▌
     python-pkgs/drive-pkg/drive/tests/test_5mph_cutoff.py ✓                                                                           71% ███████▎
     python-pkgs/drive-pkg/drive/tests/test_blanklines.py ✓                                                                            79% ███████▉
     python-pkgs/drive-pkg/drive/tests/test_empty.py ✓                                                                                 86% ████████▋
     python-pkgs/drive-pkg/drive/tests/test_example.py ✓                                                                               93% █████████▍
     python-pkgs/drive-pkg/drive/tests/test_fewdrivers.py ✓                                                                           100% ██████████

    ---------- coverage: platform darwin, python 2.7.15-final-0 ----------
    Name                                                      Stmts   Miss  Cover
    -----------------------------------------------------------------------------
    python-pkgs/drive-pkg/drive/__init__.py                       9      1    89%
    python-pkgs/drive-pkg/drive/cli.py                           34     12    65%
    python-pkgs/drive-pkg/drive/core/__init__.py                  2      0   100%
    python-pkgs/drive-pkg/drive/core/command.py                  74      4    95%
    python-pkgs/drive-pkg/drive/core/reporting.py                70      0   100%
    python-pkgs/drive-pkg/drive/metadata.py                       6      0   100%
    python-pkgs/drive-pkg/drive/tests/__init__.py                 0      0   100%
    python-pkgs/drive-pkg/drive/tests/test_0.py                   2      0   100%
    python-pkgs/drive-pkg/drive/tests/test_100mph_cutoff.py       7      0   100%
    python-pkgs/drive-pkg/drive/tests/test_5mph_cutoff.py         7      0   100%
    python-pkgs/drive-pkg/drive/tests/test_blanklines.py          7      0   100%
    python-pkgs/drive-pkg/drive/tests/test_empty.py               7      0   100%
    python-pkgs/drive-pkg/drive/tests/test_example.py             7      0   100%
    python-pkgs/drive-pkg/drive/tests/test_fewdrivers.py          7      0   100%
    python-pkgs/drive-pkg/drive/util.py                          25      7    72%
    -----------------------------------------------------------------------------
    TOTAL                                                       264     24    91%


    Results (0.24s):
          14 passed

If the coverage isn't ~100%, I recommend viewing the interactive coverage report:

    coverage html; open htmlcov/index.html

This opens a static web page where you can browse files and see which lines aren't covered.


# Updating requirements.txt

If you plan to install the package to another machine or virtualenv using
`pip install`, the `requirements.txt` file adjacent to `setup.py` needs to be
kept up to date with the package's library needs. Here's a simple process:

1. Keep a copy of the current `requirements.txt` 

2. Review your virtualenv's currently-installed packages:

    `$ pip freeze`

3. Remove any packages that are clearly unneeded:

    `$ pip uninstall UNWANTED-PKG-NAME`
        
4. Capture the installed package names and versions:

    `$ pip freeze >requirements.txt`

    The results will look like:

        ...
        PyYAML==3.11
        records==0.5.2
        requests==2.12.4
        requests-ntlm==1.0.0
        rsa==3.4.2
        s3transfer==0.1.9
        scandir==1.4
        scipy==0.19.1
        ...


5. There may be packages listed that are NOT needed to run your package, even
if you need them in your dev / test environment for other reasons -- csvkit,
pylint, etc. Remove these lines so they don't bloat your install. Packages
noted in `setup.py` / `TESTLIBS` should NOT be listed in `requirements.txt`.


6. Edit `requirements.txt` to loosen version requirements, so this package
doesn't overly constrain other packages' choice of libraries. Usually we want
to allow the same version or higher within the same major version, e.g.:

    `requests==2.12.4`

    should become:
    
    `requests>=2.12.4,<3.0.0`

7. You may need to vary from this pattern, e.g. if you know a specific point
version is required.  If you saved a copy of the prior `requirements.txt` from
step 1 above, you can use that to see what special choices were made before.


# Updating metadata.py

The `__version__` entry in metadata.py is the overall version for the package.
This doesn't need to change while you're doing dev-local changes, but as soon
as you're ready for any of the package files to leave your computer, bump it:

* Before a `git push` to a remote repo
* Before publishing to Gemfury or Cloudsmith
* Before using in a Docker image


Use semantic versioning -- see https://semver.org/


# Updating setup.py

The `setup.py` script is mostly boilerplate, and will work as-is for a wide
variety of packages as long as the "module-specific settings" section has been
updated. Most of what setup.py does is dictated by your `requirements.txt` file.


# Publishing to Cloudsmith

You can use Cloudsmith to allow simple `pip` installs of the package.

1. Bump the `__version__` entry in metadata.py if not already

2. Create a source distribution:

    `$ cdproject`
    
    `$ cd python-pkgs/drive-pkg`
    
    `$ python setup.py sdist`

3. Verify package name and version are expected:

    `$ ls -l ./dist`
    
    `-rw-r--r-- 1 chris staff 29940 Nove 27 06:50 johnson-drive-1.0.7.tar.gz`

4. Publish to the Cloudsmith service:

    `$ cloudsmith push python chris-johnson/public ./dist/johnson-drive-1.0.7.tar.gz`

5. Now any machine with the right pip configuration can install this package via pip:

    `$ pip install johnson-drive==1.0.7 --extra-index-url=https://dl.cloudsmith.io/public/chris-johnson/public/python/index/`
