# drive - driving history report utility

Chris Johnson, 28-Nov-2018

See https://gitlab.com/chris-johnson/johnson-drive

---------------------------------------

# 1. Quick Start

## 1.1. End Users

[optional] If you use `virtualenvwrapper`, create a virtualenv:

    mkproject johnson-drive

Install the package:

    pip install johnson-drive==1.0.7 --extra-index-url=https://dl.cloudsmith.io/public/chris-johnson/public/python/index/

Run the utility:

        drive <YOURDATA.txt


## 1.2. Developers via Git

[optional] If you use `virtualenvwrapper`, create a virtualenv:

    mkproject johnson-drive

Clone the repo, either from Gitlab or the git bundle:

    ## Gitlab
    git clone https://gitlab.com/chris-johnson/johnson-drive.git .  ## NOTE THE DOT
    
    ## OR

    ## Git bundle
    git clone johnson-drive.bundle .  ## NOTE THE DOT

Install the package:

    pip install ./python-pkgs/drive-pkg/

[optional] Run the test suite:

    python ./python-pkgs/drive-pkg/setup.py test

See the developer notes in `./python-pkgs/drive-pkg/README.md` for more discussion of test tools,
including coverage analysis and an interactive coverage browsing tool that's very useful.

Run the utility:

    drive <YOURDATA.txt


---------------------------------------

# 2. Solution Discussion

## 2.1. High-Level Objectives

Although the given requirements are simple, it's easy to see how this problem
could grow and change over time. My high-level goal is to meet the current
requirements with a solution that's fast & simple enough now (not over-built),
and that can be extended easily if and when new requirements emerge (flexible
but not pre-built, YAGNI principle).


## 2.2. Design Overview

### Modularization - CLI and Core

The CLI utility and core functionality (data parsing, calculations etc.) are
delivered as separate modules. The CLI is just one possible client of the core
-- the core could also be used by GUI apps, Lambda workers, Slackbots, placed
behind a REST API -- can be reused in any UI / context / channel. 90% of the
functionality, and dev-test effort, is in the core.

In this approach, the core doesn't tailor its output or exceptions for the
caller. Core raises exceptions as needed. The CLI or other callers can
translate those to an appropriate error message for the user / channel. I
haven't spent time on making the CLI error message super-friendly.


### Modularization - Test Suite

Python has a rich variety of testing tools. I'm using a mix of source-embedded
doctests (best for simple, localized, context-free testing, like at a function
level) and separate PyTest test scripts (best for more complex cases with dependency
injection, test fixtures etc.). Both types of tests are included in the overall
regression test command, along with a code-coverage analysis.

The current tests have >90% code coverage. In a real-world situation, I'd strive for 100% test coverage. 


### Large / Streaming Inputs

I assumed the input data can be very large in cases. The solution doesn't
require all input records to be stored to generate a report. The
memory footprint scales with the number of distinct names (not trips) via an
efficient accumulator model. A test with 1M+ trips was not at all burdened.

The speed performance of the current solution is acceptable. For example, with ~1.1M
input commands, the process requires 1m38s on my Mac laptop. If it became
required to use much larger data sets, I'd take a fresh look at the I/O and internal data structures
aspect of the solution, depending on what the largest data set could be.


### Distribution & Installation

I've provided two ways to install and run the solution: via `git clone`
from a Gitlab repo (best for developers), and via `pip install` from a
Cloudsmith repo (best for end users). Each of these channels delivers the same
solution; just the packaging is different.

Although Python apps are often run as scripts, e.g. `python solution.py`, I
don't prefer that approach because it requires someone to install libraries and
handle other configs, either manually or via another layer of scripting - that's
inconvenient and easy to get wrong. I provided a standard Python installer so
`pip install` can handle everything. The installer is used in each of the channels
mentioned above to achieve a consistent configuration.

If it seemed useful, I would
usually also deliver a Docker image, but didn't take that step today.


### Environment Portability

The current solution is targeted for Python 2.7.9+. All my testing was done 
in Python 2.7.15. Since it's simple to install a specific Python version using
the `virtualenv` toolset, I don't see this as a big limitation. This code
could be generalized for Python 3 easily using the `six` library and other
common utilities. If support for multiple Python versions became a requirement,
there would be a ~5% code change, and I'd add a layer of `tox` tests above `pytest`.

The current solution is targeted for unix only, no Windows. The libraries I'm using
are all mainstream and well-tested, so the solution should be highly portable in unix and may
already work just fine for Windows for all we know, but I haven't tested for that. If greater
portability became a requirement, I'd add a layer of `circleci` tests above `tox`.

### Potential for Reuse

This same basic pattern could be used for a wide range of data aggregation or
transformation utilities. If we had a number of analogous problems with different
data and/or different output requirements, this package could be refactored to
provide base classes for other solutions to reuse. 80% of this solution isn't
specific to the commands, input data format or output data format, so it's a
good candidate for reuse. I haven't done anything along this lines in this version.


## 2.3. Detailed Design Decisions

### Command Line Operation

The `drive` command line uses the simple `Fire` library to process commands
and parameters. This is not as full-featured as other CLI frameworks, e.g. the
`click` library. If this was a broadly-distributed solution, I'd move off of
`Fire` to provide a better user experience. But it's well-suited for current situation.

In addition to the default `drive` command, there are basic subcommands: `drive usage`,
`drive version`, `drive help` (manpage) and `drive report` (same as just `drive`).
Unrecognized commands trigger a not-as-friendly `Fire`-default usage message.


### Data Source Abstraction

The current requirement is to accept input data from stdin. That works well in
the current environment. If it became useful to support more input sources -- e.g. 
S3, FTP, WebDAV, SQS, database etc. might be useful data sources in a production setting --
I'd use the `fs` library and similar tools to abstract the sources, which is pretty simple.


### Data Format Abstraction

The current requirement is to accept space-delimited, record-oriented data.
If it became useful to support more input formats -- JSON, CSV, etc. -- I'd
abstract the format, probably by converting all input formats to JSON on the way in and
processing just-JSON within the core.  However this flexibility isn't needed now.


### Data Interpretation

The requirements are silent on a few issues re: how to interpret the
input data. I'll document my thought process here.

* _Relationship between Driver and Trip records._  It's implied that a Driver
record registering a driver's name with the solution will be provided before
a Trip record uses that name. However with semi-structured input data, there's
no guarantee that will happen. We need to choose some way to deal with the
potential of Trip records that use all-new names.
In the real world, this is an issue I'd discuss with business owners and PMs.

    * Preprocess all Driver records?  We could sort the input and handle all
    Driver records first, in case the problem is just record ordering.
    This is not a good solution because (1) Driver records could still be
    missing after sorting; and (2) to implement this approach with large
    (streaming) data sets would be unnecessarily complicated.

    * Reject Trip records referencing unregistered driver names? This can
    be done easily if it became a requirement. Without a requirement, it feels
    heavy-handed, as long as this doesn't introduce data integrity issues.
    If Driver records provided data that isn't also provided by Trip records,
    rejecting would be the right approach. But since the current Driver
    record only provides data that's redundant with the Trip record, it doesn't seem necessary.

    * Use Trip records in lieu of Driver records? If a Trip record appears
    with a name that wasn't previously given as a Driver record, we can register
    that name from the Trip record in the same way as Driver records. Essentially we'd ignore Driver records
    other than making sure they aren't invalid in some other way. **This is my choice
    because it is tolerant of a foreseeable but harmless condition in the input data** (robustness principle).

* _Case-sensitivity of driver names_. The example data features normal person
names. Names like this, as opposed to system-generated IDs, are usually key
punched by a person, and people can introduce errors. I need to decide if
`Dan`, `dan` and `DAN` are one or three driver names. **I'm choosing to treat
the names as case-insensitive and convert to title-case**. Normalizing seems
helpful for human-input / human-readable data, and title-case will preserve
the example data as-is if I'm wrong and the data is consistent (as opposed
to e.g. converting to lowercase, which would not preserve the example output).
This is another issue that I'd discuss with business owners and PMs in the
real world. This decision is easy to change if my judgment on this is wrong.
The only edge case I'm aware of is that Unicode title-casing doesn't always
work right with the Python 2.x `title()` method, example the name `Päper`
in title-case is `PäPer`. I'm not working on that edge case right now.

* _Case sensitivity of command_. I'm choosing to treat commands as case-insensitive.


### Commands

The current requirements only include 2 commands. I've chosen to implement
the commands as classes that are 1:1 with each command / record structure. If we
had a larger or frequently-changing set of commands, I'd move these classes out
of the main reporting module into some kind of plugin mechanism (simple in
Python), so new commands could be added without having to rev the whole solution.
Although I generally like the plugin approach and it would work even for 2
commands, it's not needed now, and it's simple to change later.


### Data Structures

There are many ways the input data could be represented in internal
data structures. If there was a reason to save and reuse the input data, I'd
design a small relational database and use that both to store the interpreted
input data and to generate data for the report. However since there's
no requirement for that, I can reduce the solution complexity by not introducing
a database. I was tempted to use the SQLite embedded database as a transient
(per-run) database until I realized there's no need to store the detailed input records
even during a single run, which is important for large data sets. I settled on a simple
dictionary-based structure that uses the user name as a key, and accumulates the gross
results for each name in a single record.

With the current requirements, the only thing that would push me away from that
approach would be if the number of names became so large that the dictionary became
a memory problem, in which case I'd reimplement the Accumulator class
with a database or REDIS backend, without needing to change anything else -- good encapsulation.


### Input Validation

Although the input data structure is simple, there are data quality
rules I need to enforce: high-level format (e.g. string vs. number); low-level
format (e.g. "HH:MM" time format); value constraints (e.g. MPH >= 0); and intra-field
logic (e.g. start time <= end time). If we had many commands and fields,
I'd use a canned data validation approach: maybe convert the CSV data to
JSON and use JSON schemas; or use Pandas or any of a dozen different validation tools.

However with only 2 commands and 4 distinct field formats, that seems like overkill.
I've elected to create a simple interpreter class for each command with hand-coded rules
for data validation.  The number or complexity of commands and fields wouldn't have
to increase very much for me to want to look into the canned validation approach.


### Output Format

The requirements don't cover a couple points. (1) We are both rounding the
miles driven, and sorting by most-to-least miles. This doesn't say whether
the sort should use miles driven before or after rounding. I choose to sort
by the rounded value. (2) Two names can have the same miles driven -- we
need further sorting criteria to ensure a deterministic output. I choose
to use name as the secondary sorting key.
